var map;

// Объект для хранения наших измерений
var measure = {
    mvcLine: new google.maps.MVCArray(),
    mvcPolygon: new google.maps.MVCArray(),
    mvcMarkers: new google.maps.MVCArray(),
    line: null,
    polygon: null
};

// Инициализируем карту и ловим клики
jQuery(document).ready(function () {

    map = new google.maps.Map(document.getElementById("map"), {
        zoom: 15,
        center: new google.maps.LatLng(55.792360, 49.122224),
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        draggableCursor: "crosshair"
    });

    google.maps.event.addListener(map, "click", function (evt) {
        // Когда кликаем на карту, вызываем передаем координаты точки в соответствующий метод
        measureAdd(evt.latLng);
    });

    for (var i = 0; i < measure.mvcMarkers.length; i++)
        console.log(measure.mvcMarkers[i])

});


function measureAdd(latLng) {

    // Добавляем маркер, который можно двигать
    var marker = new google.maps.Marker({
        map: map,
        position: latLng,
        draggable: true,
        raiseOnDrag: false,
        title: "Drag me to change shape",
    });

    // Добавляем нашу точку(координаты) в линию и многоугольник
    // Объекты, добавленные в MVCArray автоматически обновляют фигуры на карте
    measure.mvcLine.push(latLng);
    measure.mvcPolygon.push(latLng);

    // Это для того, чтобы можно было удалять маркеры после измерений
    measure.mvcMarkers.push(marker);

    // Индекс точки, которую мы поместили на карту
    // Понадобится, чтобы изменить MVCArray, когда пользователь меняет положение маркера
    var latLngIndex = measure.mvcLine.getLength() - 1;


    // Когда маркер перенесен, обновляем значения в соответстующих массивах
    google.maps.event.addListener(marker, "drag", function (evt) {
        measure.mvcLine.setAt(latLngIndex, evt.latLng);
        measure.mvcPolygon.setAt(latLngIndex, evt.latLng);
    });

    // Выполняется после окончания переноса, если количество вершин больше одной
    google.maps.event.addListener(marker, "dragend", function () {
        if (measure.mvcLine.getLength() > 1) {
            measureCalc();
        }
    });

    // Если в линии более одной вершины...
    if (measure.mvcLine.getLength() > 1) {
        // ...и линия еще не создана
        if (!measure.line) {
            // Создаем линию (google.maps.Polyline)
            measure.line = new google.maps.Polyline({
                map: map,
                clickable: false,
                strokeColor: "#FF0000",
                strokeOpacity: 1,
                strokeWeight: 3,
                path: measure.mvcLine
            });

        }

        // Если в многоугольнике более 2ух вершин...
        if (measure.mvcPolygon.getLength() > 2) {

            // ..и многоугольник еще не создан
            if (!measure.polygon) {

                // Создаем его! (google.maps.Polygon)
                measure.polygon = new google.maps.Polygon({
                    clickable: false,
                    map: map,
                    fillOpacity: 0.25,
                    strokeOpacity: 0,
                    paths: measure.mvcPolygon
                });

            }

        }

    }

    // и считаем все
    if (measure.mvcLine.getLength() > 1) {
        measureCalc();
    }

}


function measureCalc() {

    // Спасибо, о Google Maps Geometry!
    var length = google.maps.geometry.spherical.computeLength(measure.line.getPath());
    jQuery("#span-length").text(length.toFixed(1))


    if (measure.mvcPolygon.getLength() > 2) {
        var area = google.maps.geometry.spherical.computeArea(measure.polygon.getPath());
        jQuery("#span-area").text(area.toFixed(1));
    }

}

function measureSave(event) {




    event.preventDefault();




}


function measureReset() {

    // Продаем все ненужное
    if (measure.polygon) {
        measure.polygon.setMap(null);
        measure.polygon = null;
    }
    if (measure.line) {
        measure.line.setMap(null);
        measure.line = null
    }

    // Продаем все из массивов
    measure.mvcLine.clear();
    measure.mvcPolygon.clear();

    // И продаем все маркеры!
    measure.mvcMarkers.forEach(function (elem, index) {
        elem.setMap(null);
    });
    measure.mvcMarkers.clear();

    jQuery("#span-length,#span-area").text(0);

}