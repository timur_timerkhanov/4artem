<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<jsp:useBean id="dateValue" class="java.util.Date"/>


<t:wrapper>
    <jsp:attribute name="mainContent">
           <!-- content -->
    <div id="content" class="site-content">
        <div id="primary" class="content-area">
            <main id="main" class="site-main" role="main">

                <div class="container">
                    <div class="row">
                        <!-- article -->
                        <div class="col-md-8">
                            <div class="site-entry">


                                <c:forEach items="${posts}" var="post">

                                    <article class="post format-standard hentry">
                                        <div class="entry-featured-image">
                                            <img src="/images/${post.getImage()}"
                                                 alt="${post.getTitle()}">
                                        </div><!-- .entry-featured-image -->

                                        <div class="entry-area">
                                            <div class="entry-header">
                                                <h2 class="entry-title">
                                                    <a href="<c:url value="/post/${post.getId()}"/> "
                                                       rel="bookmark">
                                                            ${post.getTitle()}</a></h2>
                                                <div class="entry-meta">
                            <span class="posted-on">
                              <a href="single.html" rel="bookmark">
                                  <time class="entry-date">
                                      <fmt:formatDate value="${post.getPubDate()}"
                                                      pattern="d MMM yyyy"/>

                                  </time>
                              </a>
                            </span><!-- .posted-on -->
                                                    <span class="entry-cat">
                                                        <c:forEach items="${post.getCategories()}" var="category">
                                                            <span class="cat-link"><a
                                                                    href="category.html">${category.getName()}
                                                            </a></span>
                                                        </c:forEach>

                            </span><!-- .entry-cat -->
                                                    <span class="entry-like"><a href=""><i class="fa fa-heart-o"></i> 15</a></span>
                                                </div><!-- .entry-meta -->
                                            </div><!-- .entry-header -->

                                            <div class="entry-content">
                                                <p class="text-center">
                                                        ${fn:substring(post.getText(),0 ,255  )}

                                                    <a href="<c:url value="/post/${post.getId()}"/> "
                                                       class="more-link">
                                                        <span class="moretext">Читать</span> <span
                                                            class="screen-reader-text">Healthy Breakfast For Cute Girl</span>
                                                    </a><!-- .more-link -->
                                                </p>
                                            </div><!-- .entry-content -->
                                        </div><!-- .entry-area -->
                                    </article>
                                    <!-- .post -->

                                </c:forEach>


                                <nav class="navigation posts-navigation" role="navigation">
                                    <h2 class="screen-reader-text">Posts navigation</h2>
                                    <div class="nav-links">
                                        <div class="nav-previous"><a href="#">Older posts</a></div>
                                        <div class="nav-next"><a href="#">Newer posts</a></div>
                                    </div>
                                </nav> <!-- .navigation -->
                            </div>
                        </div><!-- .col-md-8 -->

                        <!-- sidebar -->
                        <div class="col-md-4">
                            <div class="site-sidebar">
                                <section class="widget widget_search">
                                    <form class="search-form">
                                        <div class="form-group">
                                            <label for="search" class="sr-only">Search</label>
                                            <input type="text" class="form-control search-field"
                                                   placeholder="Search...">
                                        </div><!-- .form-group -->
                                        <button type="submit" class="btn btn-default search-submit"><i
                                                class="fa fa-search"></i></button>
                                    </form><!-- .search-form -->
                                </section><!-- .widget_search -->

                                <section class="widget astrif_widget_about">
                                    <h2 class="widget-title"><span>О себе</span></h2>
                                    <div class="about-author-container">
                                        <div class="author-avatar">
                                            <img class="img-responsive" src="../assets/img/astrif-about.jpg" height="200" alt="astrif fifth">
                                        </div>
                                        <div class="author-info">
                                            <p>Привет, меня зовут Тимур. Я программист и это моя семестровка.
                                                Это место, где вы можете делиться своими рецептами вкусной и здоровой
                                                еды!</p>
                                        </div>
                                        <div class="author-signature">
                                            <img class="col-md-12" src="../assets/img/signature.png" alt="">
                                        </div>
                                    </div><!-- .about-author-container -->
                                </section><!-- .astrif_widget_about -->

                                <section class="widget astrif_widget_follow_me">
                                    <h2 class="widget-title"><span>Follow me</span></h2>
                                    <div class="follow-me">
                                        <ul class="social-icon social-icon-color">
                                            <li><a href="" class="icon-twitter"><i class="fa fa-twitter"></i></a></li>
                                            <li><a href="" class="icon-facebook"><i class="fa fa-facebook"></i></a></li>
                                            <li><a href="" class="icon-instagram"><i class="fa fa-instagram"></i></a>
                                            </li>
                                            <li><a href="" class="icon-pinterest"><i class="fa fa-pinterest"></i></a>
                                            </li>
                                            <li><a href="" class="icon-youtube"><i class="fa fa-youtube-play"></i></a>
                                            </li>
                                        </ul>
                                    </div><!-- .follow-me -->
                                </section><!-- .astrif_widget_follow_me -->

                                <section class="widget astrif_widget_popular_entries">
                                    <h2 class="widget-title"><span>Popular Posts</span></h2>
                                    <ul>
                                        <li>
                                            <a href="single.html" class="popular-entry-thumbnail">
                                                <img src="../assets/img/astrif-popular-01.jpg"
                                                     alt="10 Easy Breakfast that can try in home">
                                            </a>
                                            <span class="popular-entry-title">
                            <a href="single.html" rel="bookmark" class="popular-entry-title-link">10 Easy Breakfast that can try in home</a>
                            <span class="popular-entry-date">August 17, 2016</span>
                          </span>
                                        </li>
                                        <li>
                                            <a href="single.html" class="popular-entry-thumbnail">
                                                <img src="../assets/img/astrif-popular-02.jpg"
                                                     alt="My Daily healthy food">
                                            </a>
                                            <span class="popular-entry-title">
                            <a href="single.html" rel="bookmark"
                               class="popular-entry-title-link">My Daily healthy food</a>
                            <span class="popular-entry-date">July 23, 2016</span>
                          </span>
                                        </li>
                                        <li>
                                            <a href="single.html" class="popular-entry-thumbnail">
                                                <img src="../assets/img/astrif-popular-03.jpg"
                                                     alt="Not drink a coke is good">
                                            </a>
                                            <span class="popular-entry-title">
                            <a href="single.html" rel="bookmark" class="popular-entry-title-link">Not drink a coke is better, i think</a>
                            <span class="popular-entry-date">May 8, 2016</span>
                          </span>
                                        </li>
                                        <li>
                                            <a href="single.html" class="popular-entry-thumbnail">
                                                <img src="../assets/img/astrif-popular-04.jpg"
                                                     alt="This pancake as sweet as you">
                                            </a>
                                            <span class="popular-entry-title">
                            <a href="single.html" rel="bookmark" class="popular-entry-title-link">This pancake as sweet as you :')</a>
                            <span class="popular-entry-date">January 27, 2016</span>
                          </span>
                                        </li>
                                        <li>
                                            <a href="single.html" class="popular-entry-thumbnail">
                                                <img src="../assets/img/astrif-popular-05.jpg"
                                                     alt="My daily drink in the morn">
                                            </a>
                                            <span class="popular-entry-title">
                            <a href="single.html" rel="bookmark" class="popular-entry-title-link">My daily drink in the morn</a>
                            <span class="popular-entry-date">March 11, 2015</span>
                          </span>
                                        </li>
                                    </ul>
                                </section><!-- .astrif_widget_popular_entries -->


                            </div><!-- .site-sidebar -->
                        </div><!-- .col-md-4 -->
                    </div><!-- .row -->
                </div><!-- .container -->

            </main><!-- #main -->
        </div><!-- #primary -->
    </div><!-- #content -->
    </jsp:attribute>
    <jsp:attribute name="banners">
        <footer id="colophon" class="site-footer" role="contentinfo">
            <!-- footer subscribe -->
            <div class="footer-subscribe">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
                            <h3 class="subscribe-title">Subscribe</h3>
                            <p>Subscribe your e-mail address and get fresh stuff through email notifications.</p>
                            <form class="form-inline">
                                <div class="form-group">
                                    <input type="email" class="form-control" placeholder="Your email address">
                                </div>
                                <button type="submit" class="btn btn-default">Sign up</button>
                            </form><!-- .form-inline -->
                        </div><!-- .col-## -->
                    </div><!-- .row -->
                </div><!-- .container -->
            </div><!-- .footer-subscribe -->

            <!-- footer instagram -->
            <div class="footer-instagram">
                <div class="instagram-title">
                    <h3>insta <i class="fa fa-instagram"></i> gram</h3>
                    <span class="instagram-subtitle">Know me better</span>
                </div>

                <div class="instagram-widget">
                    <ul>
                        <li><a href=""><img src="../assets/img/astrif-post-entry-02.jpg" alt=""></a></li>
                        <li><a href=""><img src="../assets/img/astrif-post-entry-03.jpg" alt=""></a></li>
                        <li><a href=""><img src="../assets/img/astrif-post-entry-04.jpg" alt=""></a></li>
                        <li><a href=""><img src="../assets/img/astrif-post-entry-05.jpg" alt=""></a></li>
                        <li><a href=""><img src="../assets/img/astrif-post-entry-06.jpg" alt=""></a></li>
                        <li><a href=""><img src="../assets/img/astrif-post-entry-07.jpg" alt=""></a></li>
                    </ul>
                    ../
                    <p class="instagram-follow"><a href="http://instagram.com/templateninja" target="_blank"
                                                   class="btn btn-default">@templateninja</a></p>
                </div>
            </div><!-- .footer-instagram -->
        </footer><!-- #site-footer -->
    </jsp:attribute>
</t:wrapper>
