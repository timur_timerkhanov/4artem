<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@attribute name="scripts" fragment="true" required="false" %>
<%@attribute name="mainContent" fragment="true" required="false" %>
<%@attribute name="banners" fragment="true" %>
<%@tag pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Astrif responsive food blogging theme">
    <meta name="keywords" content="astrif, food blog, blogging, responsive, personal blog">
    <title>ЕдаБлог</title>

    <!-- Bootstrap -->
    <link href="<c:url value="../assets/css/bootstrap.css"/> " rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700|Oswald:700" rel="stylesheet">
    <link href="<c:url value="../assets/css/font-awesome.css"/> " rel="stylesheet">
    <link href="../assets/css/owl.carousel.css" type="text/css" rel="stylesheet">
    <link href="../assets/css/owl.theme.css" type="text/css" rel="stylesheet">
    <link href="../assets/css/style.css" rel="stylesheet">
    <link href="../assets/css/skin.min.css" rel="stylesheet">

    <script src="../assets/js/jquery-3.2.1.js"></script>

</head>
<body class="archive category">
<div id="page" class="site">
    <a class="skip-link screen-reader-text" href="#content">Skip to content</a>

    <!-- site branding -->
    <div class="site-branding">
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <ul class="social-icon">
                        <li><a href="" class="icon-twitter"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="" class="icon-facebook"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="" class="icon-instagram"><i class="fa fa-instagram"></i></a></li>
                        <li><a href="" class="icon-pinterest"><i class="fa fa-pinterest"></i></a></li>
                        <li><a href="" class="icon-youtube"><i class="fa fa-youtube-play"></i></a></li>
                    </ul>
                </div><!-- .col-md-3 -->

                <div class="col-md-6">
                    <h1 class="site-title title-image"><a href="index.html" rel="home"><img
                            src="<c:url value="../assets/img/logo.png"/>" alt="TimCookBook"></a></h1>
                    <p class="site-description">- Вкусная и здоровая еда -</p>
                </div><!-- .col-md-6 -->

                <div class="col-md-3">
                    <form class="search-form">
                        <div class="form-group">
                            <label for="search" class="sr-only">Search</label>
                            <input type="text" class="form-control search-field" placeholder="Search...">
                        </div><!-- .form-group -->
                        <button type="submit" class="btn btn-default search-submit"><i class="fa fa-search"></i>
                        </button>
                    </form><!-- .search-form -->
                </div><!-- .col-md-3 -->
            </div><!-- .row -->
        </div><!-- .container -->
    </div><!-- .site-branding -->

    <!-- main navigation -->
    <div class="site-navigation-sticky-wrapper">
        <header id="masthead" class="site-header" role="banner">
            <nav id="site-navigation" class="main-navigation" role="navigation">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false"><i
                                    class="fa fa-bars"></i><span class="sr-only">Primary Menu</span></button>
                            <div class="menu-container">
                                <ul id="primary-menu" class="menu nav-menu" aria-expanded="false">
                                    <li class="menu-item current-menu-item"><a href="<c:url value="/"/>">Home</a>
                                    </li>
                                    <li class="menu-item menu-item-has-children" aria-haspopup="true"><a href="#">
                                        Категории</a>
                                        <ul class="sub-menu">
                                            <li class="menu-item"><a href="single.html">Single Post</a></li>
                                            <li class="menu-item"><a href="single-sidebar.html">Single Post With
                                                Sidebar</a></li>
                                            <li class="menu-item"><a href="page-fullwidth.html">Page Full Width</a></li>
                                            <li class="menu-item"><a href="page-sidebar.html">Page With Sidebar</a></li>
                                            <li class="menu-item"><a href="shortcodes.html">Shortcodes</a></li>
                                            <li class="menu-item"><a href="typography.html">Typography</a></li>
                                            <li class="menu-item"><a href="404.html">Not Found</a></li>
                                        </ul>
                                    </li>
                                    <li class="menu-item current-menu-item"><a href="category.html">Food</a></li>
                                    <li class="menu-item"><a href="category.html">Cook</a></li>
                                    <li class="menu-item"><a href="about.html">About Me</a></li>
                                    <c:if test="${not empty user}">
                                        <li class="menu-item"><a href="about.html">${user.getName()}</a></li>

                                    </c:if>
                                    <li class="menu-item"><a href="<c:url value="/add"/> ">Создать рецепт</a></li>
                                </ul>
                            </div><!-- .menu-container -->
                        </div><!-- .col-md-12 -->
                    </div><!-- .row -->
                </div><!-- .container -->
            </nav><!-- #site-navigation -->
        </header><!-- #masthead -->
    </div><!-- .site-header-affix-wrapper -->


    <jsp:doBody/>

    <jsp:invoke fragment="mainContent"/>


    <jsp:invoke fragment="banners"/>
    <!-- copyright -->
    <section id="copyright" class="copyright">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    Copyright &copy; 2016 <a href="http://templateninja.net" target="_blank"><strong>Тимур</strong></a>.
                    Все права сохранены.
                </div><!-- .col-sm-6 -->
            </div><!-- .row -->
        </div><!-- .container -->
    </section><!-- #copyright -->

</div><!-- #page -->

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->

<script src="../assets/js/bootstrap.js"></script>
<script src="../assets/js/imagesloaded.pkgd.min.js"></script>
<script src="../assets/js/owl.carousel.js"></script>
<script src="../assets/js/masonry.pkgd.min.js"></script>
<script src="../assets/js/navigation.js"></script>
<script src="../assets/js/skip-link-focus-fix.js"></script>
<script src="../assets/js/script.min.js"></script>

<jsp:invoke fragment="scripts"/>
</body>
</html>