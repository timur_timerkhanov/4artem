<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href=<c:url value="../assets/css/styleForMarkers.css"/>/>
</head>

<body>



<div id="map" style="height:33em"></div>

<div class="wrapper">


    <div class="info" style="margin-top:15px;">
        <b>LENGTH</b>
        <span id="span-length">
            </span>
    </div>


    <br>
    <div class="info">
        <b>AREA</b>
        <span id="span-area"></span>
    </div>
</div>


<a href="#" onclick="measureReset()" class="btn purple">Reset</a>

<a id="save" class="btn green">Save</a>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script type="text/javascript" src="https://maps.google.com/maps/api/js?key=AIzaSyCZCu_ywYYgurFijJokQMG7ZlN-FdP8GJk&libraries=geometry">
</script>

<script src="<c:url value="../assets/js/script1.js"/> " type="text/javascript">
</script>

<script>

    var lat = [];
    var lng = [];

    for (var i = 0; i < measure.mvcMarkers.getLength(); i++) {
        lat[i] = (measure.mvcMarkers.getAt(i).getPosition().lat());
        lng[i] = (measure.mvcMarkers.getAt(i).getPosition().lng());
    }

    $(document).ready(function () {
        $("#save").click(function (e) {
            e.preventDefault();
            $.ajax(
                {
                    url: "/measuresave",
                    type: "POST",
                    data: {
                        lng: lng,
                        lat: lat
                    },
                    success: function () {
                        console.log("Hooray!")
                    },
                    error: function () {
                        console.log("Error!")
                    }
                });
        });
    });


</script>
</body>

</html>