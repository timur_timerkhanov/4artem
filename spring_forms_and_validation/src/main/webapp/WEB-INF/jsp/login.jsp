<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<t:wrapper>
    <jsp:body>
        <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="panel panel-primary">
                <div class="panel-heading" id="my_panel_head">
                    <h3 class="panel-title">Пожалуйста, войдите</h3>
                </div>

                <div class="panel-body">
                    <form method="POST" action="${contextPath}/login" class="col-md-6 col-md-offset-3">

                        <div class="form-group ${error != null ? 'has-error' : ''}">

                            <c:if test="${message != null}">
                                <div class="alert alert-success">
                                        ${message}
                                </div>
                            </c:if>


                            <input name="email" type="text" class="form-control" placeholder="Email"
                                   autofocus="true"/>
                            <div class="spacing"></div>
                            <input name="password" type="password" class="form-control" placeholder="Пароль"/>
                            <div class="spacing"></div>
                            <span>${error}</span>
                            <div class="spacing"></div>

                            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>

                            <button class="btn btn-primary colored-button pull-right" type="submit">Log In</button>
                            <h4 class="text-center"><a href="${contextPath}/registration">Создать аккаунт</a></h4>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </jsp:body>
</t:wrapper>
