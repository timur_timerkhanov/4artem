<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<jsp:useBean id="dateValue" class="java.util.Date"/>


<t:wrapper>
    <jsp:attribute name="mainContent">
<div id="page" class="site">
    <a class="skip-link screen-reader-text" href="#content">Skip to content</a>

    <!-- content -->
    <div id="content" class="site-content">
        <div id="primary" class="content-area">
            <main id="main" class="site-main" role="main">

                <div class="container">
                    <div class="row">
                        <!-- article -->
                        <div class="col-md-8 col-md-offset-2">
                            <div class="site-entry">

                                <article class="post format-standard hentry">
                                    <div class="entry-header">
                                        <h1 class="entry-title cust-title">${post.getTitle()}</h1>
                                        <div class="entry-meta">
                          <span class="posted-on">
                            <a href="single.html" rel="bookmark"> <time class="entry-date"> <fmt:formatDate
                                    value="${post.getPubDate()}"
                                    pattern="d MMM yyyy"/></time> </a>
                          </span><!-- .posted-on -->
                                            <span class="entry-comment">
                            <span class="comment-link"><a href="#comments">4 Comments</a></span>
                          </span><!-- .entry-cat -->
                                            <span class="entry-cat">
                              <c:forEach items="${post.getCategories()}" var="category">
                                                            <span class="cat-link"><a
                                                                    href="category.html">${category.getName()}
                                                            </a></span>
                                                        </c:forEach>
                          </span><!-- .entry-cat -->
                                            <span class="entry-like"><a href=""><i
                                                    class="fa fa-heart-o"></i> 42</a></span>
                                            <c:if test="${post.getAuthor().getId() == user.getId()}">


                                                    <form action="<c:url value="delete/${post.getId()}"/>" style="margin-top: 1em;" id="delete"
                                                          class="form-inline" method="post">
                                                        <span>
                                                <a href="<c:url value="edit/${post.getId()}"/> "
                                                   class="btn btn-primary btn-sm">Изменить</a>
                                            </span>
                                                        <input type="submit" id="delete_button"
                                                               class="btn btn-danger btn-sm" value="Удалить">
                                                    </form>


                                        </c:if>
                                        </div><!-- .entry-meta -->
                                    </div><!-- .entry-header -->

                                    <div class="entry-featured-image text-center">
                                        <img style="margin-left: 50%; margin-bottom: 2em;"
                                             src="/images/${post.getImage()}"
                                             alt="${post.getTitle()}">
                                    </div><!-- .entry-featured-image -->


                                    <div class="entry-content">
                                            ${post.getText()}
                                    </div><!-- .entry-content -->

                                    <footer class="entry-footer">
                        <span class="tags-links">
                          <a href="#">recipes</a> <a href="#">cook</a> <a href="#">food</a> <a href="#">appetizer</a>
                        </span><!-- .tags-links -->
                                        <span class="share-links">
                          <span>Share this</span>
                          <a href="" class="icon-facebook"><i class="fa fa-facebook"></i> Facebook</a>
                          <a href="" class="icon-twitter"><i class="fa fa-twitter"></i> Twitter</a>
                          <a href="" class="icon-google-plus"><i class="fa fa-google-plus"></i> Google+</a>
                          <a href="" class="icon-pinterest"><i class="fa fa-pinterest"></i> Pinterest</a>
                          <a href="" class="icon-linkedin"><i class="fa fa-linkedin"></i> LinkedIn</a>
                        </span><!-- .share-links -->
                                    </footer><!-- .entry-footer -->
                                </article><!-- .post -->

                                <nav class="navigation post-navigation" role="navigation">
                                    <h2 class="screen-reader-text">Post navigation</h2>
                                    <div class="nav-links">
                                        <div class="nav-previous">
                                            <a href="#" class="image-link"><img
                                                    src="assets/img/astrif-post-entry-02.jpg"></a>
                                            <span>Previous Post</span>
                                            <a href="#" class="prev-link" rel="prev">Healthy Breakfast For Cute Girl</a>
                                        </div>
                                        <div class="nav-next">
                                            <a href="#" class="image-link"><img
                                                    src="assets/img/astrif-post-entry-07.jpg"></a>
                                            <span>Next Post</span>
                                            <a href="#" class="next-link" rel="next">Gelato in 'omah gelato' will punch
                                                you hard!</a>
                                        </div>
                                    </div>
                                </nav><!-- .navigation -->

                                <!-- author bio -->
                                <div class="section-title">
                                    <h3>Кто есть автор?</h3>
                                </div>
                                <div class="entry-author-bio">
                                    <div class="author-bio-avatar">
                                        <img alt="author bio" src="assets/img/astrif-about.jpg" class="avatar">
                                    </div><!-- .author-bio-avatar -->
                                    <div class="author-bio-description">
                                        <h4>${post.getAuthor().getName()}</h4>
                                        <p>${post.getAuthor().getAbout()}</p>
                                        <small> &mdash; ${post.getAuthor().getActivity()}</small>

                                        <ul class="social-icon">
                                            <li><a href="#" class="icon-facebook" target="_blank"><i
                                                    class="fa fa-facebook"></i></a></li>
                                            <li><a href="#" class="icon-twitter" target="_blank"><i
                                                    class="fa fa-twitter"></i></a></li>
                                            <li><a href="#" class="icon-instagram" target="_blank"><i
                                                    class="fa fa-instagram"></i></a></li>
                                            <li><a href="#" class="icon-youtube" target="_blank"><i
                                                    class="fa fa-youtube-play"></i></a></li>
                                            <li><a href="#" class="icon-pinterest" target="_blank"><i
                                                    class="fa fa-pinterest"></i></a></li>
                                        </ul>
                                    </div><!-- .author-bio-description -->
                                </div><!-- entry-author-bio -->

                                <div class="related-posts">
                                    <div class="section-title">
                                        <h3>You May Also Like</h3>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <div class="post-container">
                                                <div class="post-thumbnail">
                                                    <a href="#"><img src="assets/img/astrif-post-entry-02.jpg"></a>
                                                </div><!-- .post-thumbnail -->
                                                <h3 class="post-title"><a href="#">What I Wore Today</a></h3>
                                                <span class="post-meta"><a href="#">Okt 09, 2016</a></span>
                                            </div><!-- .post-container -->
                                        </div><!-- .col-md-3 -->
                                        <div class="col-sm-4">
                                            <div class="post-container">
                                                <div class="post-thumbnail">
                                                    <a href="#"><img src="assets/img/astrif-post-entry-04.jpg"></a>
                                                </div><!-- .post-thumbnail -->
                                                <h3 class="post-title"><a href="#">10 comfy swimsuit in this month</a>
                                                </h3>
                                                <span class="post-meta"><a href="#">Aug 14, 2016</a></span>
                                            </div><!-- .post-container -->
                                        </div><!-- .col-md-3 -->
                                        <div class="col-sm-4">
                                            <div class="post-container">
                                                <div class="post-thumbnail">
                                                    <a href="#"><img src="assets/img/astrif-post-entry-05.jpg"></a>
                                                </div><!-- .post-thumbnail -->
                                                <h3 class="post-title"><a href="#">7 sweet hat for hangout that you
                                                    should know</a></h3>
                                                <span class="post-meta"><a href="#">Aug 02, 2016</a></span>
                                            </div><!-- .post-container -->
                                        </div><!-- .col-md-3 -->
                                    </div><!-- .row -->
                                </div><!-- .related-posts -->

                                <div id="comments" class="comments-area">
                                    <div class="section-title">
                                        <h3>4 Comments</h3>
                                    </div>

                                    <nav id="comment-nav-above" class="navigation comment-navigation" role="navigation">
                                        <h2 class="screen-reader-text">Comment navigation</h2>
                                        <div class="nav-links">
                                            <div class="nav-previous">
                                                <a href="#comments">Older Comments</a>
                                            </div>
                                            <div class="nav-next">
                                                <a href="#comments">Newer Comments</a>
                                            </div>
                                        </div><!-- .nav-links -->
                                    </nav><!-- #comment-nav-above -->

                                    <ol class="comment-list">
                                        <li class="comment even parent">
                                            <article class="comment-body">
                                                <footer class="comment-meta">
                                                    <div class="comment-author vcard">
                                                        <img alt="" src="assets/img/astrif-comment-author-01.jpg"
                                                             class="avatar">
                                                        <span class="fn">Jessica</span>
                                                    </div><!-- .comment-author -->

                                                    <div class="comment-metadata">
                                                        <a href="#">
                                                            <time datetime="2016-05-09T00:51:13+00:00"> May 9, 2016 at
                                                                11:21 am
                                                            </time>
                                                        </a>
                                                    </div><!-- .comment-metadata -->
                                                </footer><!-- .comment-meta -->

                                                <div class="comment-content">
                                                    <p>Nowadays, a family is simply a network of people who care for
                                                        each other. It can contain hundreds or two. You can be born into
                                                        one or build your own.</p>
                                                </div><!-- .comment-content -->

                                                <div class="reply">
                                                    <a rel="nofollow" class="comment-reply-link"
                                                       href="#respond">Reply</a>
                                                </div>
                                            </article><!-- .comment-body -->

                                            <ol class="children">
                                                <li class="comment even parent">
                                                    <article class="comment-body">
                                                        <footer class="comment-meta">
                                                            <div class="comment-author vcard">
                                                                <img alt=""
                                                                     src="assets/img/astrif-comment-author-02.jpg"
                                                                     class="avatar">
                                                                <span class="fn">Jane Doe</span>
                                                            </div><!-- .comment-author -->

                                                            <div class="comment-metadata">
                                                                <a href="#">
                                                                    <time datetime="2016-05-09T00:51:13+00:00"> May 9,
                                                                        2016 at 12:51 am
                                                                    </time>
                                                                </a>
                                                            </div><!-- .comment-metadata -->
                                                        </footer><!-- .comment-meta -->

                                                        <div class="comment-content">
                                                            <p>Nowadays, a family is simply a network of people who care
                                                                for each other. It can contain hundreds or two. You can
                                                                be born into one or build your own.</p>
                                                        </div><!-- .comment-content -->

                                                        <div class="reply">
                                                            <a rel="nofollow" class="comment-reply-link"
                                                               href="#respond">Reply</a>
                                                        </div>
                                                    </article><!-- .comment-body -->

                                                    <ol class="children">
                                                        <li class="comment even parent">
                                                            <article class="comment-body">
                                                                <footer class="comment-meta">
                                                                    <div class="comment-author vcard">
                                                                        <img alt=""
                                                                             src="assets/img/astrif-comment-author-03.jpg"
                                                                             class="avatar">
                                                                        <span class="fn">Diana Brown</span>
                                                                    </div><!-- .comment-author -->

                                                                    <div class="comment-metadata">
                                                                        <a href="#">
                                                                            <time datetime="2016-05-09T00:51:13+00:00">
                                                                                May 9, 2016 at 12:51 am
                                                                            </time>
                                                                        </a>
                                                                    </div><!-- .comment-metadata -->
                                                                </footer><!-- .comment-meta -->

                                                                <div class="comment-content">
                                                                    <p>Nowadays, a family is simply a network of people
                                                                        who care for each other. It can contain hundreds
                                                                        or two. You can be born into one or build your
                                                                        own.</p>
                                                                </div><!-- .comment-content -->

                                                                <div class="reply">
                                                                    <a rel="nofollow" class="comment-reply-link"
                                                                       href="#respond">Reply</a>
                                                                </div>
                                                            </article><!-- .comment-body -->

                                                            <ol class="children">
                                                                <li class="comment even parent">
                                                                    <article class="comment-body">
                                                                        <footer class="comment-meta">
                                                                            <div class="comment-author vcard">
                                                                                <img alt=""
                                                                                     src="assets/img/astrif-comment-author-04.jpg"
                                                                                     class="avatar">
                                                                                <span class="fn">Briana Smith</span>
                                                                            </div><!-- .comment-author -->

                                                                            <div class="comment-metadata">
                                                                                <a href="#">
                                                                                    <time datetime="2016-05-09T00:51:13+00:00">
                                                                                        May 9, 2016 at 12:51 am
                                                                                    </time>
                                                                                </a>
                                                                            </div><!-- .comment-metadata -->
                                                                        </footer><!-- .comment-meta -->

                                                                        <div class="comment-content">
                                                                            <p>Nowadays, a family is simply a network of
                                                                                people who care for each other. It can
                                                                                contain hundreds or two. You can be born
                                                                                into one or build your own.</p>
                                                                        </div><!-- .comment-content -->

                                                                        <div class="reply">
                                                                            <a rel="nofollow" class="comment-reply-link"
                                                                               href="#respond">Reply</a>
                                                                        </div>
                                                                    </article><!-- .comment-body -->
                                                                </li>
                                                            </ol>
                                                        </li>
                                                    </ol>
                                                </li>
                                            </ol>
                                        </li>
                                    </ol><!-- .comment-list -->

                                    <nav id="comment-nav-below" class="navigation comment-navigation" role="navigation">
                                        <h2 class="screen-reader-text">Comment navigation</h2>
                                        <div class="nav-links">
                                            <div class="nav-previous">
                                                <a href="#comments">Older Comments</a>
                                            </div>
                                            <div class="nav-next">
                                                <a href="#comments">Newer Comments</a>
                                            </div>
                                        </div><!-- .nav-links -->
                                    </nav><!-- #comment-nav-below -->

                                    <div id="respond" class="comment-respond">
                                        <div class="section-title">
                                            <h3>Leave a comment</h3>
                                        </div>
                                        <form class="form-horizontal comment-form">
                                            <div class="form-group">
                                                <div class="col-sm-12">
                                                    <label>Message</label>
                                                    <textarea class="form-control" rows="6"></textarea>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-4">
                                                    <label>Name *</label>
                                                    <input type="text" class="form-control">
                                                </div>
                                                <div class="col-sm-4">
                                                    <label>Email *</label>
                                                    <input type="email" class="form-control">
                                                </div>
                                                <div class="col-sm-4">
                                                    <label>Website</label>
                                                    <input type="url" class="form-control">
                                                </div>
                                            </div>
                                            <p class="form-submit">
                                                <input name="submit" type="submit" class="btn btn-default"
                                                       value="Submit">
                                            </p>
                                        </form>
                                    </div><!-- .comment-respond -->
                                </div><!-- .comments-area -->
                            </div><!-- .site-entry -->
                        </div><!-- .col-md-8 -->
                    </div><!-- .row -->
                </div><!-- .container -->

            </main><!-- #main -->
        </div><!-- #primary -->
    </div><!-- #content -->

</div><!-- #page -->

              <script>

                      $(function() {
                          $("#delete_button").click(function(){
                              if (confirm("Click OK to continue?")){
                                  $('#delete').submit();
                              }
                          });
                      });
              </script>
        </jsp:attribute>

</t:wrapper>