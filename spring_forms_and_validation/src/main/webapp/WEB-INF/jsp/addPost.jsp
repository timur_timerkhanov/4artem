<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<t:wrapper>
    <jsp:attribute name="scripts">
            <sec:csrfMetaTags/>
    <meta name="_csrf_parameter" content="_csrf"/>
    <meta name="_csrf_header" content="X-CSRF-TOKEN"/>
    <meta name="_csrf" content="e62835df-f1a0-49ea-bce7-bf96f998119c"/>
        <link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.3/summernote.css" rel="stylesheet">
        <script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.3/summernote.js"></script>
        <script src="<c:url value="../assets/js/summernote.js"/> "></script>


        <script type="text/javascript">
            function validateForm() {
                var a = document.forms["Form"]["title"].value;
                if (a == null || a == "") {
                    alert("Please Fill All Required Field");
                    return false;
                }
            }
        </script>
    </jsp:attribute>


    <jsp:body>
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <form:form method="post" name="Form" modelAttribute="postForm" enctype="multipart/form-data"
                               onsubmit="return validateForm()">
                        <div class="form-group">
                            <label for="title"><spring:message code="posts.title"/></label>
                            <form:input path="title" type="text" name="title" class="form-control" id="title"/>
                        </div>

                        <div class="form-group">
                            <label for="editor"><spring:message code="posts.text_ru"/>:</label>
                            <form:textarea path="text" class="form-control" id="editor" name="text_ru" rows="15"/>
                        </div>

                        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                        <div class="form-group">
                            <label for="category">Категория</label>
                            <form:select multiple="true"
                                         path="categories" items="${map}" name="categories" id="category"
                                         class="form-control" type="text"/>
                        </div>

                        <div class="form-group">
                            <label for="image">Изображение
                            </label>
                            <form:input path="image" id="image" class="form-control" type="file"/>

                        </div>

                        <div class="text-center">
                            <button type="submit" class="btn btn-primary">
                                <spring:message code="posts.save"/>
                            </button>
                        </div>

                    </form:form>
                </div>
            </div>
        </div>
    </jsp:body>

</t:wrapper>
