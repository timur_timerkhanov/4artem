<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<t:wrapper>
    <jsp:body>
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <div class="panel panel-primary">
                    <div class="panel-heading" id="my_panel_head">
                        <h3 class="panel-title">Регистрация</h3>
                    </div>


                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-10 col-md-offset-1">
                                <form:form method="post" enctype="multipart/form-data"
                                           commandName="authorForm" cssClass="registrationForm">

                                    <spring:bind path="email">
                                        <div class="form-group spacing">
                                            <form:label path="email" cssClass="col-md-3 control-label">
                                                <spring:message code="registration.email"/>
                                            </form:label>
                                            <div class="col-md-12">
                                                <form:input path="email" type="text" cssClass="form-control input-sm"/>
                                            </div>
                                            <br>
                                        </div>
                                        <div class="errors">
                                            <form:errors path="email"/>
                                        </div>
                                    </spring:bind>
                                    <div class="spacing"></div>


                                    <spring:bind path="password">
                                        <div class="form-group spacing">
                                            <form:label path="password" cssClass="col-md-3 control-label">
                                                <spring:message code="registration.password"/>
                                            </form:label>
                                            <div class="col-md-12">
                                                <form:input path="password" type="password"
                                                            cssClass="form-control input-sm"/>
                                            </div>
                                            <br>
                                        </div>
                                        <div class="errors">
                                            <form:errors path="password"/>
                                        </div>
                                    </spring:bind>
                                    <div class="spacing"></div>


                                    <spring:bind path="confirmPassword">
                                        <div class="form-group spacing">
                                            <form:label path="confirmPassword" cssClass="col-md-3 control-label">
                                                <spring:message code="registration.password"/>
                                            </form:label>
                                            <div class="col-md-12">
                                                <form:input path="confirmPassword" type="password"
                                                            cssClass="form-control input-sm"/>
                                            </div>
                                            <br>
                                        </div>
                                        <div class="errors">
                                            <form:errors path="password"/>
                                        </div>
                                    </spring:bind>
                                    <div class="spacing"></div>


                                    <spring:bind path="name">
                                        <div class="form-group spacing">
                                            <form:label path="name" cssClass="col-md-3 control-label">
                                                <spring:message code="registration.full_name"/>
                                            </form:label>
                                            <div class="col-md-12">
                                                <form:input path="name" type="text" cssClass="input-sm form-control"/>
                                            </div>
                                            <br>

                                        </div>

                                        <div class="errors">
                                            <form:errors path="name"/>
                                        </div>
                                    </spring:bind>
                                    <div class="spacing"></div>


                                    <spring:bind path="city">
                                        <div class="form-group spacing">
                                            <form:label path="city" cssClass="col-md-3 control-label">
                                                Город
                                            </form:label>
                                            <div class="col-md-12">
                                                <form:input path="city" cssClass="form-control input-sm"/>
                                            </div>
                                        </div>
                                    </spring:bind>
                                    <br>

                                    <div class="form-group spacing">
                                        <label for="files" class="col-md-3 control-label">
                                            Аватар
                                        </label>
                                        <div class="col-md-12">
                                            <input name="files" id="files" type="file" class="input-sm"/>
                                        </div>
                                    </div>

                                    <br>


                                    <div class="spacing"></div>
                                    <spring:bind path="about">
                                        <div class="form-group spacing">
                                            <form:label path="about" cssClass="col-md-3 control-label">
                                                О себе
                                            </form:label>
                                            <div class="col-md-12">
                                                <form:textarea path="about" rows="10" cssClass="form-control"/>
                                            </div>
                                        </div>
                                    </spring:bind>
                                    <div class="spacing"></div>

                                    <div class="spacing"></div>

                                    <div class="form-group">
                                        <div class="text-center">
                                            <input type="submit" style="margin-top: 1em"
                                                   value="<spring:message code="registration.register"/>"
                                                   class="btn btn-primary colored-button">
                                        </div>
                                    </div>
                                </form:form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>


    </jsp:body>
</t:wrapper>