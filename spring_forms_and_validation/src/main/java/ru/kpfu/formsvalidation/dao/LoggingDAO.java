package ru.kpfu.formsvalidation.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.kpfu.formsvalidation.model.Logging;


public interface LoggingDAO extends JpaRepository<Logging, Integer> {

}
