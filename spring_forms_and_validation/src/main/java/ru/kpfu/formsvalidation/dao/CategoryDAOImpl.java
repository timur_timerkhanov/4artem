package ru.kpfu.formsvalidation.dao;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.kpfu.formsvalidation.model.Category;
import ru.kpfu.formsvalidation.model.Post;

import java.util.List;

@Repository
@Transactional
public class CategoryDAOImpl implements CategoryDAO {
    @Autowired
    private SessionFactory sessionFactory;

    public CategoryDAOImpl() {

    }

    public CategoryDAOImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }


    @Override
    public List<Category> list() {
        @SuppressWarnings("unchecked")
        List<Category> listPost = (List<Category>) sessionFactory.getCurrentSession()
                .createCriteria(Category.class)
                .setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
        return listPost;
    }

    @Override
    public Category get(int id) {
        return (Category) sessionFactory.getCurrentSession().get(Category.class, id);
    }

    @Override
    public void delete(int id) {

    }
}
