package ru.kpfu.formsvalidation.dao;

import ru.kpfu.formsvalidation.model.Category;
import ru.kpfu.formsvalidation.model.Post;

import java.util.List;

public interface CategoryDAO {
    public List<Category> list();

    public Category get(int id);

    public void delete(int id);
}
