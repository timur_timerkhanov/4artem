package ru.kpfu.formsvalidation.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.kpfu.formsvalidation.model.Author;

public interface AuthorDAO extends JpaRepository<Author, Integer>, AuthorDAOCustom {

}
