package ru.kpfu.formsvalidation.dao;

import ru.kpfu.formsvalidation.model.Author;

public interface AuthorDAOCustom {
    Author findAuthorByEmail(String email);
}
