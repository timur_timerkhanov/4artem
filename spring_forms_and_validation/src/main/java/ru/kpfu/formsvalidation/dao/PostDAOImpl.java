package ru.kpfu.formsvalidation.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.kpfu.formsvalidation.loggers.Loggable;
import ru.kpfu.formsvalidation.model.Post;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

@Repository
@Transactional
public class PostDAOImpl implements PostDAO {
    @Autowired
    private SessionFactory sessionFactory;

    @PersistenceContext
    private EntityManager em;

    public PostDAOImpl() {
    }

    public PostDAOImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Loggable
    @Override
    public List<Post> list() {
        System.out.println("You are on list() method");
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Post> cq = cb.createQuery(Post.class);
        Root<Post> rootEntry = cq.from(Post.class);
        CriteriaQuery<Post> all = cq.select(rootEntry);
        TypedQuery<Post> allQuery = em.createQuery(all);
        return allQuery.getResultList();
    }

    @Override
    public Post get(int id) {
        return (Post) sessionFactory.getCurrentSession().get(Post.class, id);
    }

    @Override
    public void saveOrUpdate(Post post) {
        sessionFactory.getCurrentSession().saveOrUpdate(post);
    }

    @Override
    public void delete(int id) {
        Session session = sessionFactory.getCurrentSession();
        Post post = (Post) session.load(Post.class, id);
        post.getCategories().clear();
        session.delete(post);
        session.flush();
    }
}
