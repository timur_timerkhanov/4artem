package ru.kpfu.formsvalidation.dao;

import ru.kpfu.formsvalidation.model.Author;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;


public class AuthorDAOImpl implements AuthorDAOCustom {

    @PersistenceContext
    private EntityManager em;
    @Override
    public Author findAuthorByEmail(String email) {
        Author author = em.createQuery(
                "SELECT a from Author a WHERE a.email = :email", Author.class).
                setParameter("email", email).getSingleResult();
        return author;
    }
}
