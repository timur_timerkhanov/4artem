package ru.kpfu.formsvalidation.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.kpfu.formsvalidation.model.Role;

public interface RoleDAO extends JpaRepository<Role, Integer> {
}
