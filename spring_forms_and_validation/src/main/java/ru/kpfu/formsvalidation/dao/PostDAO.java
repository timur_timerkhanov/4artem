package ru.kpfu.formsvalidation.dao;

import ru.kpfu.formsvalidation.model.Post;

import java.util.List;

public interface PostDAO {
    public List<Post> list();

    public Post get(int id);

    public void saveOrUpdate(Post post);

    public void delete(int id);
}