package ru.kpfu.formsvalidation.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import ru.kpfu.formsvalidation.dao.AuthorDAO;
import ru.kpfu.formsvalidation.dao.RoleDAO;
import ru.kpfu.formsvalidation.model.Author;
import ru.kpfu.formsvalidation.model.Role;

import java.util.HashSet;
import java.util.Set;

@Service
public class AuthorService {
    @Autowired
    private AuthorDAO authorDAO;

    @Autowired
    private RoleDAO roleDAO;

    private BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();

    public void saveAuthor(Author author) {
        author.setPassword(bCryptPasswordEncoder.encode(author.getPassword()));
        Set<Role> roleSet = new HashSet<>();
        roleSet.add(roleDAO.getOne(1));
        author.setRoles(roleSet);
        authorDAO.save(author);
    }

    public Author getAuthorByEmail(String email) {
        return authorDAO.findAuthorByEmail(email);
    }

}
