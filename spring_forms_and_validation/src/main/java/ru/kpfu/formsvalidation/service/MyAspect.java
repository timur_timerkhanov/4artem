package ru.kpfu.formsvalidation.service;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import ru.kpfu.formsvalidation.model.Logging;

import java.sql.Timestamp;

@Aspect
public class MyAspect {
    @Autowired
    private LoggingService loggingService;

    @Around("execution(* *(..)) && @annotation(ru.kpfu.formsvalidation.loggers.Loggable)")
    public Object loggingAdvice(ProceedingJoinPoint joinPoint) throws Throwable {
        Object proceed = joinPoint.proceed();
        System.out.println("*********");
        Logging logging = new Logging();
        Object[] args = joinPoint.getArgs();
        logging.setName(joinPoint.getSignature().getName());
        logging.setTime(new Timestamp(System.currentTimeMillis()));
        logging.setArgs("1");
        loggingService.writeLog(logging);
        System.out.println(joinPoint.getSignature().getName());

        System.out.println("*********");
        return proceed;
    }
}