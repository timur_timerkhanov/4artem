package ru.kpfu.formsvalidation.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import ru.kpfu.formsvalidation.dao.LoggingDAO;
import ru.kpfu.formsvalidation.model.Logging;

@Service
public class LoggingService {
    @Autowired
    private LoggingDAO loggingDAO;
    public void writeLog(Logging logging) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();

        if (auth.getPrincipal() != null)
            System.out.println(auth.getPrincipal().toString());
        loggingDAO.saveAndFlush(logging);
    }
}
