package ru.kpfu.formsvalidation.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.kpfu.formsvalidation.dao.AuthorDAO;
import ru.kpfu.formsvalidation.model.Author;
import ru.kpfu.formsvalidation.model.Role;
import ru.kpfu.formsvalidation.security.AuthorUser;

import java.util.HashSet;
import java.util.Set;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {
    @Autowired
    private AuthorDAO authorDAO;


    @Override
    @Transactional(readOnly = true)
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        AuthorUser authorUser = null;

        Author author = authorDAO.findAuthorByEmail(email);

        Set<GrantedAuthority> grantedAuthorities = new HashSet<>();
        for (Role role : author.getRoles()) {
            grantedAuthorities.add(new SimpleGrantedAuthority(role.getName()));
        }
        return new AuthorUser(author.getEmail(), author.getPassword(), true, true, true, true,
                grantedAuthorities, author.getId(), author.getEmail(), author.getName());
    }
}
