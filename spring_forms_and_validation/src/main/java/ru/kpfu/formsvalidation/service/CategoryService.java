package ru.kpfu.formsvalidation.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.kpfu.formsvalidation.dao.CategoryDAO;
import ru.kpfu.formsvalidation.model.Category;

import java.util.List;

@Service
public class CategoryService {
    @Autowired
    private CategoryDAO categoryDAO;

    public List<Category> getAllCategories() {
        return categoryDAO.list();
    }

    public Category get(int id) {
        return categoryDAO.get(id);
    }
}
