package ru.kpfu.formsvalidation.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.MultiValueMap;
import ru.kpfu.formsvalidation.converters.AbstractTwoWayConverter;
import ru.kpfu.formsvalidation.converters.IntegerToEntityConverter;
import ru.kpfu.formsvalidation.dao.CategoryDAO;
import ru.kpfu.formsvalidation.dao.PostDAO;
import ru.kpfu.formsvalidation.loggers.Loggable;
import ru.kpfu.formsvalidation.model.Author;
import ru.kpfu.formsvalidation.model.Category;
import ru.kpfu.formsvalidation.model.Post;

import java.sql.Timestamp;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class PostService {
    @Autowired
    private PostDAO postDAO;
    @Autowired
    private IntegerToEntityConverter integerToEntityConverter;

    public void saveOrUpdate(Post post) {
        postDAO.saveOrUpdate(post);
    }

    public Post postFromRequest(MultiValueMap<String, String> attributes, Author author) {
        Post post = new Post();
        post.setTitle(attributes.get("title").get(0));
        post.setText(attributes.get("text").get(0));
        post.setPubDate(new Timestamp(System.currentTimeMillis()));
        post.setImage(attributes.get("imageURL").get(0));
        post.setAuthor(author);
        Set<Category> categoriesSet = new HashSet<>(0);

        for (String cat : attributes.get("categories")) {
            categoriesSet.add(integerToEntityConverter.convert(Integer.valueOf(cat)));
        }
        post.setCategories(categoriesSet);
        return post;
    }

    @Loggable
    public List<Post> getAllPosts() {
        return postDAO.list();
    }

    public Post getSinglePost(Integer id) {
        return postDAO.get(id);
    }

    public void deletePost(Integer id) {
        postDAO.delete(id);
    }
}
