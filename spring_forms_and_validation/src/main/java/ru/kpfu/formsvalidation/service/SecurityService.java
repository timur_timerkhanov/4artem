package ru.kpfu.formsvalidation.service;

import ru.kpfu.formsvalidation.security.AuthorUser;

public interface SecurityService {
    AuthorUser findLoggedInByEmail();

    void autoLogin(String email, String password);
}
