package ru.kpfu.formsvalidation.service;

import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;

@Service
public class FileUploadService {
    public String uploadSingleFile(MultipartFile file) {
        try {
            byte[] bytes = file.getBytes();
            String name = file.getOriginalFilename().split("\\.")[0];
            String ext = file.getOriginalFilename().split("\\.")[1];
            // Creating the directory to store file
            String rootPath = "/home/timur/IdeaProjects/spring_forms_and_validation/staticfiles";
            File dir = new File(rootPath + File.separator + "images");
            if (!dir.exists())
                dir.mkdirs();

            // Create the file on server
            File serverFile = new File(dir.getAbsolutePath()
                    + File.separator + name + "." + ext);
            BufferedOutputStream stream = new BufferedOutputStream(
                    new FileOutputStream(serverFile));
            stream.write(bytes);
            stream.close();
            //TODO: REMOVE THIS HARDCODING PATH
            return name + "." + ext;
//            return serverFile.getAbsolutePath();
        } catch (Exception e) {
            e.printStackTrace();
            return "You failed to upload => " + e.getMessage();
        }
    }
}

