package ru.kpfu.formsvalidation.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import ru.kpfu.formsvalidation.loggers.Loggable;
import ru.kpfu.formsvalidation.model.Author;
import ru.kpfu.formsvalidation.model.Category;
import ru.kpfu.formsvalidation.model.Post;
import ru.kpfu.formsvalidation.security.AuthorUser;
import ru.kpfu.formsvalidation.service.AuthorService;
import ru.kpfu.formsvalidation.service.CategoryService;
import ru.kpfu.formsvalidation.service.FileUploadService;
import ru.kpfu.formsvalidation.service.PostService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
public class SpringController {

    @Autowired
    private PostService postService;
    @Autowired
    private CategoryService categoryService;
    @Autowired
    private FileUploadService fileUploadService;
    @Autowired
    private AuthorService authorService;

    @GetMapping("/admin")
    public String admin() {
        return "admin";
    }

    private Author getCurrentAuthor() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        AuthorUser user = null;
        Author author = null;

        if (!(auth instanceof AnonymousAuthenticationToken)) {
            if (auth.getPrincipal() instanceof String) {
                author = authorService.getAuthorByEmail(auth.getPrincipal().toString());
            } else {
                user = (AuthorUser) auth.getPrincipal();
                author = authorService.getAuthorByEmail(user.getEmail());
            }
        }
        return author;
    }


    @GetMapping("/markers")
    public String markers() {
        return "markers";
    }

    @PostMapping("/measuresave")
    public String markersSave() {
        return "home";
    }

    @Loggable
    @RequestMapping(value = {"/main", "/"})
    public String mainPage(Model model) {
        Author user = getCurrentAuthor();
        model.addAttribute("posts", postService.getAllPosts());
        model.addAttribute("user", user);
        return "main";
    }

    @GetMapping("/add")
    public String addPostGET(ModelMap modelMap) {

        List<Category> list = categoryService.getAllCategories();
        Map<Integer, String> map = new HashMap<>();
        for (Category i : list) map.put(i.getId(), i.getName());
        modelMap.put("map", map);

        Post post = new Post();
        modelMap.addAttribute("postForm", post);
        return "addPost";
    }

    @PostMapping({"/add", "/post/edit/{id}"})
    public String addPostPOST(@RequestParam(required = false, value = "image") MultipartFile file,
                              @PathVariable(required = false) Integer id,
                              @RequestParam MultiValueMap<String, String> allRequestParams) {
        allRequestParams.add("imageURL", fileUploadService.uploadSingleFile(file));

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        AuthorUser user = null;
        if (!(auth instanceof AnonymousAuthenticationToken)) {
            user = (AuthorUser) auth.getPrincipal();
        }
        Post post = postService.postFromRequest(allRequestParams, authorService.getAuthorByEmail(user.getEmail()));
        if (id != null) {
            post.setId(id);
        }
        postService.saveOrUpdate(post);
        return "redirect:/main";
    }

    @GetMapping("/post/{id}")
    public String singlePost(@PathVariable Integer id, ModelMap modelMap) {
        modelMap.addAttribute("post", postService.getSinglePost(id));
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        AuthorUser user = null;
        if (!(auth instanceof AnonymousAuthenticationToken)) {
            user = (AuthorUser) auth.getPrincipal();
        }
        modelMap.addAttribute("user", user);
        return "single";
    }

    @GetMapping("/post/edit/{id}")
    public String editPost(@PathVariable Integer id, ModelMap modelMap) {
        List<Category> list = categoryService.getAllCategories();
        Map<Integer, String> map = new HashMap<>();
        for (Category i : list) map.put(i.getId(), i.getName());
        modelMap.put("map", map);
        modelMap.addAttribute("postForm", postService.getSinglePost(id));
        return "editPost";
    }


    @PostMapping("/post/delete/{id}")
    public ModelAndView deletePost(@PathVariable Integer id) {
        postService.deletePost(id);
        return new ModelAndView("redirect:/");
    }


//    @RequestMapping(value = "/posts")
//    public String listPosts(ModelMap model) {
//        try {
//            Locale locale = LocaleContextHolder.getLocale();
//
//            List<Post> posts = languageResolverService.getPostByLanguage(locale);
//
//            model.put("posts", posts);
//        } catch (DataIntegrityViolationException e) {
//            e.printStackTrace();
//        }
//        return "postList";
//    }
//
//    @RequestMapping(value = "/posts/managing", method = RequestMethod.GET)
//    public String allPosts(ModelMap model) throws IOException {
//        try {
//            Locale locale = LocaleContextHolder.getLocale();
//
//            List<Post> posts = languageResolverService.getPostByLanguage(locale);
//
//            model.put("posts", posts);
//        } catch (DataIntegrityViolationException e) {
//            e.printStackTrace();
//        }
//        return "allPosts";
//    }
//
//    @RequestMapping(value = "/posts/managing", method = RequestMethod.POST)
//    public String deletePost(@RequestParam Map<String, String> allRequestParams) {
//        String result = "";
//        if (allRequestParams.get("delete") != null) {
//            postService.delete(Integer.valueOf(allRequestParams.get("id")));
//            result = "redirect:/posts";
//        } else if (allRequestParams.get("edit") != null) {
//            result = "redirect:/posts/edit/" + allRequestParams.get("id");
//        }
//        return result;
//    }
//
//    @RequestMapping(value = "/posts/edit/{id}", method = RequestMethod.GET)
//    public String editPostGET(@PathVariable String id, ModelMap modelMap) {
//        Post post = postService.get(Integer.valueOf(id));
//        if (post != null) {
//            modelMap.put("post", post);
//            return "postEdit";
//        } else
//            return "404";
//    }

//    @RequestMapping(value = "/posts/edit/{id}", method = RequestMethod.POST)
//    public String editPostPOST(@PathVariable String id,
//                               @RequestParam Map<String, String> allRequestParams) {
//        Post post = postService.get(Integer.valueOf(id));
//        post.setTitle(allRequestParams.get("title"));
//        post.setTextRu(allRequestParams.get("text_ru"));
//        post.setTextEn(allRequestParams.get("text_en"));
//
//        Slugify slg = new Slugify();
//        String result = slg.slugify(allRequestParams.get("title"));
//
//        post.setSlug(result);
//
//        postService.saveOrUpdate(post);
////        Post post = postService.saveOrUpdate(slug);
//        return "redirect:/posts";
//    }
//
//    @RequestMapping(value = "/posts/add", method = RequestMethod.GET)
//    public String newPost() {
//        return "addPost";
//    }
//
//    @RequestMapping(value = "/posts/add", method = RequestMethod.POST)
//    public String addPost(@RequestParam Map<String, String> allRequestParams, ModelMap modelMap) {
//
//        Post post = new Post();
//        post.setTitle(allRequestParams.get("title"));
//
//        post.setTextEn(allRequestParams.get("text_en"));
//        post.setTextRu(allRequestParams.get("text_ru"));
//
//        Slugify slg = new Slugify();
//        String result = slg.slugify(allRequestParams.get("title"));
//        post.setSlug(result);
//        Calendar calendar = Calendar.getInstance();
//        java.util.Date now = calendar.getTime();
//        java.sql.Timestamp currentTimestamp = new java.sql.Timestamp(now.getTime());
//        post.setPubDate(currentTimestamp);
//        try {
//            postService.saveOrUpdate(post);
//        } catch (DuplicateKeyException e) {
//            int min = 1;
//            int max = 9;
//            int randomNum = ThreadLocalRandom.current().nextInt(min, max + 1);
//            post.setSlug(result + String.valueOf(randomNum));
//            postService.saveOrUpdate(post);
//        }
//
//        return "redirect:/posts/pages/1";
//    }
//
//    @RequestMapping(value = "/posts/{id}", method = RequestMethod.GET)
//    public String showPost(@PathVariable Integer id, ModelMap modelMap) {
//
//        Locale ruLocale = new Locale("ru");
//        Locale enLocale = new Locale("en");
//        Locale locale = LocaleContextHolder.getLocale();
//
//        Post post = postService.get(id);
//        if (locale.equals(ruLocale)) {
//            post.setText(post.getTextRu());
//        } else {
//            if (locale.equals(enLocale)) {
//                post.setText(post.getTextEn());
//            }
//        }
//
//        if (post != null) {
//            modelMap.put("post", post);
//            return "post";
//        } else
//            return "404";
//    }
//
//
//    // Registration
//
//    @RequestMapping(value = {"/register"}, method = RequestMethod.GET)
//    public String register(Locale locale, ModelMap modelMap) {
//        modelMap.put("user", new User());
//        populateDefaultModel(modelMap);
//        return "registration";
//    }
//
//    @RequestMapping(value = "/register", method = RequestMethod.POST)
//    public String saveOrUpdateUser(
//            RedirectAttributes redirectAttributes,
//            @Validated @ModelAttribute("user") User user,
//            BindingResult result,
//            ModelMap model) {
//
//        if (result.hasErrors()) {
//            populateDefaultModel(model);
//            return "registration";
//        } else {
//            // Add message to flash scope
//            redirectAttributes.addFlashAttribute("css", "success");
//            redirectAttributes.addFlashAttribute("msg", "User added successfully!");
//            return "redirect:/success/";
//        }
//    }
//
//
//    private void populateDefaultModel(ModelMap modelMap) {
//        Map<String, String> sex = new HashMap<>();
//        sex.put("M", "Male");
//        sex.put("F", "Female");
//        modelMap.put("sexList", sex);
//
//        Map<String, String> country = new HashMap<>();
//        country.put("US", "United Stated");
//        country.put("CHINA", "China");
//        country.put("RU", "Russia");
//        country.put("IT", "Italy");
//        modelMap.put("countryList", country);
//    }
//
//
//
//
//    @RequestMapping(value = "/posts/pages/{pageNumber}", method = RequestMethod.GET)
//    public String getRunbookPage(@PathVariable Integer pageNumber, ModelMap model) {
//        Page<Post> page = postService.getDeploymentLog(pageNumber);
//
//        int current = page.getNumber() + 1;
//        int begin = Math.max(1, current - 5);
//        int end = Math.min(begin + 10, page.getTotalPages());
//
//
//
//        model.addAttribute("posts", page.iterator());
//        model.addAttribute("beginIndex", begin);
//        model.addAttribute("endIndex", end);
//        model.addAttribute("currentIndex", current);
//
//        return "postList";
//    }
}

