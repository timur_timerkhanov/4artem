package ru.kpfu.formsvalidation.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import ru.kpfu.formsvalidation.model.Author;
import ru.kpfu.formsvalidation.service.*;
import ru.kpfu.formsvalidation.validator.AuthorValidator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Controller
public class AuthController {
    @Autowired
    private FileUploadService fileUploadService;
    @Autowired
    private SecurityService securityService;
    @Autowired
    private AuthorValidator authorValidator;
    @Autowired
    private AuthorService authorService;


    @RequestMapping(value = "/registration", method = RequestMethod.GET)
    public String registration(Model model) {
        model.addAttribute("authorForm", new Author());
        return "registration";
    }

    @RequestMapping(value = "/registration", method = RequestMethod.POST)
    public String registration(@ModelAttribute("authorForm") Author authorForm,
                               @RequestParam(required = false, value = "photo") MultipartFile file,
                               BindingResult bindingResult,
                               Model model) {
        authorValidator.validate(authorForm, bindingResult);

        authorForm.setPhoto(fileUploadService.uploadSingleFile(file));

        if (bindingResult.hasErrors()) {
            return "registration";
        }

        authorService.saveAuthor(authorForm);
        securityService.autoLogin(authorForm.getEmail(), authorForm.getConfirmPassword());
        return "redirect:main";
    }


    @GetMapping("login")
    public String login(Model model, String error, String logout) {

        // if Authenticated
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (!(auth instanceof AnonymousAuthenticationToken)) {
            return "redirect:/main";
        }
        if (error != null) {
            model.addAttribute("error", "Введены неправильные данные");
        }
        if (logout != null) {
            model.addAttribute("message", "Вы вышли");
        }
        return "login";
    }

    @RequestMapping(value = "/logout", method = RequestMethod.GET)
    public String logoutPage(HttpServletRequest request, HttpServletResponse response) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null) {
            new SecurityContextLogoutHandler().logout(request, response, auth);
        }
        return "redirect:/login?logout";//You can redirect wherever you want, but generally it's a good practice to show login screen again.
    }

}
