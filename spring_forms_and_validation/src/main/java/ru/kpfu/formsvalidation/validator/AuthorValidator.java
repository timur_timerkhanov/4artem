package ru.kpfu.formsvalidation.validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import ru.kpfu.formsvalidation.model.Author;
import ru.kpfu.formsvalidation.service.AuthorService;

@Component
public class AuthorValidator implements Validator {
    @Autowired
    private AuthorService authorService;

    @Override
    public boolean supports(Class<?> aClass) {
        return Author.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        Author author = (Author)o;

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "email", "validation.required");
    }
}
