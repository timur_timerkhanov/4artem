package ru.kpfu.formsvalidation.converters;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.kpfu.formsvalidation.model.Category;
import ru.kpfu.formsvalidation.service.CategoryService;



@Component
public class IntegerToEntityConverter extends AbstractTwoWayConverter<Integer, Category> {
    @Autowired
    private CategoryService categoryService;

    // TODO add exceptions check
    @Override
    public Category convert(Integer id) {
        return categoryService.get(id);
    }

    @Override
    public Integer convertBack(Category target) {
        return target.getId();
    }

}

